##项目说明:
####页面框架:adminlte:   
>https://adminlte.io/  
demo.html为各种组建的使用demo 不要修改

####nginx配置:
* 1.nginx启动:  
cd /usr/local/Cellar/nginx/1.15.12/bin  
sudo ./nginx 

* 2.nginx.conf:(目录:/usr/local/etc/nginx)
````
#user  nobody;
worker_processes  1;

#error_log  logs/error.log;
#error_log  logs/error.log  notice;
#error_log  logs/error.log  info;

#pid        logs/nginx.pid;


events {
    worker_connections  1024;
}


http {
    include       mime.types;
    default_type  application/octet-stream;

    #log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
    #                  '$status $body_bytes_sent "$http_referer" '
    #                  '"$http_user_agent" "$http_x_forwarded_for"';

    #access_log  logs/access.log  main;

    sendfile        on;
    #tcp_nopush     on;

    #keepalive_timeout  0;
    keepalive_timeout  65;

    #gzip  on;

    server {
        listen       8080;
        server_name  localhost;

        #charset koi8-r;

        #access_log  logs/host.access.log  main;

        location / {
            root   html;
            index  index.html index.htm;
        }

        #error_page  404              /404.html;

        # redirect server error pages to the static page /50x.html
        #
        error_page   500 502 503 504  /50x.html;
        location = /50x.html {
            root   html;
        }

        # proxy the PHP scripts to Apache listening on 127.0.0.1:80
        #
        #location ~ \.php$ {
        #    proxy_pass   http://127.0.0.1;
        #}

        # pass the PHP scripts to FastCGI server listening on 127.0.0.1:9000
        #
        #location ~ \.php$ {
        #    root           html;
        #    fastcgi_pass   127.0.0.1:9000;
        #    fastcgi_index  index.php;
        #    fastcgi_param  SCRIPT_FILENAME  /scripts$fastcgi_script_name;
        #    include        fastcgi_params;
        #}

        # deny access to .htaccess files, if Apache's document root
        # concurs with nginx's one
        #
        #location ~ /\.ht {
        #    deny  all;
        #}
    }


    # another virtual host using mix of IP-, name-, and port-based configuration
    #
    #server {
    #    listen       8000;
    #    listen       somename:8080;
    #    server_name  somename  alias  another.alias;

    #    location / {
    #        root   html;
    #        index  index.html index.htm;
    #    }
    #}


    # HTTPS server
    #
    #server {
    #    listen       443 ssl;
    #    server_name  localhost;

    #    ssl_certificate      cert.pem;
    #    ssl_certificate_key  cert.key;

    #    ssl_session_cache    shared:SSL:1m;
    #    ssl_session_timeout  5m;

    #    ssl_ciphers  HIGH:!aNULL:!MD5;
    #    ssl_prefer_server_ciphers  on;

    #    location / {
    #        root   html;
    #        index  index.html index.htm;
    #    }
    #}
    include servers/*;
}
````

* 3.servers/guazi.conf （目录:/usr/local/etc/nginx/servers）

````
upstream sf.sss.com {
    server 127.0.0.1:8000;
}
server {
    listen       80;
    server_name  dev.guazi-corp.com;
    access_log /Users/samson/Work/aidg/h5/aidg-backend-h5/logs/ac.log;
    error_log /Users/samson/Work/aidg/h5/aidg-backend-h5/logs/error.log;
    location /api {
        rewrite  ^/api/(.*)$ /$1 break;
        proxy_pass http://sf.sss.com;
        add_header Cache-Control no-cache;
        add_header Cache-Control private;
    }
    location / {
        root   /Users/samson/Work/aidg/h5/aidg-backend-h5/pages;
        index  login.html login.htm;
        add_header Cache-Control no-cache;
    }
    location ~.*(js|css|png|gif|jpg|ico|jpeg|mp3|ogg|otf|eot|svg|ttf|woff|woff2|less|scss)$ {
        root /Users/samson/Work/aidg/h5/aidg-backend-h5/;
        add_header Cache-Control no-cache;
    }
}
````
####host配置:
> 位置/etc/hosts
````
127.0.0.1  dev.guazi-corp.
````
####说明
````
127.0.0.1:10080是后端服务地址  
访问:http://dev.guazi-corp.com 会直接跳转到login.html  
当ajax访问/api/test实际通过nginx对应的是127.0.0.1:8000/test接口  
如果不配置host 直接访问http://127.0.0.1 会直接跳转到login.html
````

