$.extend(true, $.fn.dataTable.defaults, {
        language: {
            "sProcessing": "处理中...",
            "sLengthMenu": "_MENU_ ",
            "sZeroRecords": "没有匹配结果",
            "sInfo": "显示第 _START_ 至 _END_ 项结果，共 _TOTAL_ 项",
            "sInfoEmpty": "显示第 0 至 0 项结果，共 0 项",
            "sInfoFiltered": "(由 _MAX_ 项结果过滤)",
            "sInfoPostFix": "",
            "search": "_INPUT_",
            "searchPlaceholder": "搜索",
            "sUrl": "",
            "sEmptyTable": "表中数据为空",
            "sLoadingRecords": "载入中...",
            "sInfoThousands": ",",
            "oPaginate": {
                "sFirst": "首页",
                "sPrevious": "上页",
                "sNext": "下页",
                "sLast": "末页"
            },
            "oAria": {
                "sSortAscending": ": 以升序排列此列",
                "sSortDescending": ": 以降序排列此列"
            }
        },
        dom:
        "<'row'<'col-sm-6'<'toolbar'>><'col-sm-6'<'pull-right'f><'pull-right'l>C>>" +
        "<'row'<'col-sm-12'tr>>" +
        "<'row'<'col-sm-5'i><'col-sm-7'p>>",
        processing: true,
        lengthMenu: [ 10, 20, 50, 100 ],
        pageLength: 10
        /*        "searching": false,
         "ordering": false*/
});