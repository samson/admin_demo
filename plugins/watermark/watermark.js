(function (global, factory) {
    typeof exports === 'object' && typeof module !== 'undefined' ? module.exports = factory(require('extend')) :
        typeof define === 'function' && define.amd ? define('watermark', ['extend'], factory) :
            (global.watermark = factory(global.extend));
}(this, (function (extend) { 'use strict';


    var version = "1.0.1";

    /**
     * @module watermark
     * @author xujian1
     * @description 水印工具
     */

    var defaultOptions = {
        color: 'rgba(150,150,150, 0.5)',
        autoRelative: true,
        zIndex: '9999',
        top: '0',
        left: '0',
        position: 'absolute',
        width: '100%',
        height: '100%'

        /**
         *
         * @param {string} selector 要加水印的元素选择器
         * @param {string} text 水印内容
         * @param {object} [options={}] - 选项
         * @param {string} [options.color=rgba(150,150,150, 0.3)] - 水印文字颜色
         * @param {boolean} [options.autoRelative=true] - 是否将选择的元素设置为 relative 布局
         * @param {string} [options.zIndex=9999] - 水印 z-index
         * @param {string} [options.top=0]- 水印样式 top
         * @param {string} [options.left=0] - 水印样式 left
         * @param {string} [options.width=100%] - 水印样式 width
         * @param {string} [options.height=100%] - 水印样式 height
         * @param {string} [options.position=absolute] - 水印样式 position
         */
    };function watermark(selector, text, options) {
        try {
            options = Object.assign({}, defaultOptions, options);
            Array.prototype.forEach.call(document.querySelectorAll(selector), function (el) {

                if (options.autoRelative && window.getComputedStyle(el).position === 'static') {
                    el.style.position = 'relative';
                }

                el.style.opacity = options.opacity;
                var mark = document.createElement('div');
                mark.style.position = options.position;
                mark.style.width = options.width;
                mark.style.height = options.height;
                mark.style.top = options.top;
                mark.style.left = options.left;
                mark.style.zIndex = options.zIndex;
                mark.style.backgroundRepeat = 'repeat';
                mark.style.pointerEvents = 'none';

                var canvas = document.createElement('canvas');
                canvas.width = 200;
                canvas.height = 200;
                var ctx = canvas.getContext('2d');
                ctx.fillStyle = options.color;
                var font = 40;
                ctx.font = font + 'px Arial';
                ctx.translate(0, font);
                ctx.rotate(45 / 180 * Math.PI);
                while (ctx.measureText(text).width > (200 - 40) * 1.414) {
                    font -= 1;
                    ctx.font = font + 'px Arial';
                }
                ctx.fillText(text, 0, 0);
                mark.style.backgroundImage = 'url(' + canvas.toDataURL() + ')';
                el.appendChild(mark);
            });
        } catch (e) {
            // nothing
        }
    }

    /**
     * 代码版本
     * @type {String} version
     */
    watermark.version = version;

    return watermark;

})));