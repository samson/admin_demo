//
$.fn.serializeObject = function (all) {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name] !== undefined) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            if (this.value != '' || all) {
                o[this.name] = this.value || '';
            }
        } else {
            if (this.value != '' || all) {
                o[this.name] = this.value || '';
            }
        }
    });
    return o;
};

