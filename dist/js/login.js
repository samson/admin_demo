$(function () {

    //控制checkbox Remeber Me
    $('input').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%' // optional
    });

    //Ajax用户登陆
    $("button[type='button']").click(function () {
        var param = {};
        param.username = $("input[type='tel']").val();
        param.password = $("input[type='password']").val();
        request.ajaxPostJson(backend.common.login, JSON.stringify(param), null, function (respData, respMsg) {
            if (respData) {
                //登陆成功 将凭证写cookie
                $.cookie(CONFIG.CERTIFICATION, respData);
                window.location = "index.html";
            } else {
                tip.errorToastBody("未获取凭证");
            }
        }, function (respStatus, respData, respMsg) {
            tip.errorToastBody(respMsg);
        }, function (respMsg) {
            tip.errorToastBody(respMsg);
        })
    });

});

var loginObj = {
    /**
     * 正则检验邮箱
     * email 传入邮箱
     * return true 表示验证通过
     */
    checkEmail: function (email) {
        if (/^[\w\-\.]+@[\w\-]+(\.[a-zA-Z]{2,4}){1,2}$/.test(email)) {
            return true;
        }
    }
}