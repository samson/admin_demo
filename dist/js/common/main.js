/**
 * USER:    suncg
 * TIME:    2018-01-10 10:35
 * COMMENT: 公共初始化页面事件
 */
$(function () {

    //全屏控件事件
    $('.fullscreen-toggle').on('click', function (e) {
        e.preventDefault();
        if (screenfull.enabled) {
            screenfull.toggle();
        }
    });

    //增加水印
    watermark('.content', "后台系统",{
        color: 'rgba(150,150,150, 0.15)',
        autoRelative: true,
        zIndex: '9999',
        top: '0',
        left: '0',
        position: 'absolute',
        width: '100%',
        height: '100%'
    });

});




