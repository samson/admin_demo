/**
 * USER:    suncg
 * TIME:    2018-01-10 10:35
 * COMMENT: datatable封装工具
 */
var dtutil = {

    /**
     * 初始化datatable返回数据
     * @param draw
     * @returns {{}}
     */
    createReturnData: function (draw) {
        var returnData = {};
        returnData.draw = draw;
        returnData.recordsTotal = 0;
        returnData.recordsFiltered = 0;
        var datas = new Array();
        returnData.data = datas;
        return returnData;
    },

    /**
     * 初始化分页参数
     * @param data
     * @returns {{}}
     */
    createDtPageParam: function (data) {
        var param = {};
        param.start = data.start;//开始的记录序号
        param.pageSize = data.length;//页面显示记录条数，在页面显示每页显示多少项的时候
        param.pageNum = (data.start / data.length) + 1;//当前页码
        return param;
    },

    /**
     * dataTable fnDraw
     * @param table
     */
    dataTableFnDraw: function (table) {
        if (typeof(table.fnDraw) != 'undefined') {
            table.fnDraw();
        }
    }

}