requirejs.config({
    'paths': {
        'jquery': '/plugins/jquery/jquery.min',
        'bootstrap': '/plugins/bootstrap/js/bootstrap.min',
        'adminlte': '/plugins/adminlte/adminlte.min',
        'AdminLET': '/plugins/adminlte/AdminLET.demo',
        'pace': '/plugins/pace/pace.min',
        'jquery.toast': '/plugins/toast/jquery.toast.min',
        'screenfull': '/plugins/screenfull/screenfull.min',
        'jquery.datatable': '/plugins/datatable/jquery.dataTables.min',
        'dataTables.bootstrap': '/plugins/datatable/dataTables.bootstrap.min',
        'dataTables.bootstraps.common': '/plugins/datatable/common',
        'json2': '/plugins/json2/json2',
        'serialize2obj': '/plugins/jquery/jquery.extend.serialize2obj',
        'bootstrap-datetimepicker': '/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker',
        'bootstrap-datetimepicker.zh-CN': '/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.zh-CN',
        'bootbox': '/plugins/bootbox/bootbox',
        'icheck': '/plugins/icheck/icheck.min',
        'bootstrap-treeview': '/plugins/bootstrap-treeview/bootstrap-treeview.min',
        'jquery.form': '/plugins/form/jquery.form',
        'ajaxfileupload': '/plugins/ajaxfileupload/ajaxfileupload',
        'watermark': '/plugins/watermark/watermark',

        'dist.common.common': '/dist/js/common/common',
        'dist.common.main': '/dist/js/common/main',
        'dist.common.api': '/dist/js/api',
        'dist.common.lib': '/dist/js/common/lib',
        'dist.common.dt.util': '/dist/js/common/dt.util',
        'dist.common.constants': '/dist/js/common/constants',
        'dist.common.request': '/dist/js/common/request',
        'dist.common.tip': '/dist/js/common/tip',
        'dist.common.bttree.util': '/dist/js/common/bttree.util',
        'dist.common.demo': '/dist/js/demo',


    },
    shim: {
        'bootstrap': ['jquery'],
        'adminlte': ['jquery'],
        'AdminLET': ['jquery'],
        'pace': ['jquery'],
        'jquery.toast': ['jquery'],
        'screenfull': ['jquery'],
        'jquery.datatable': ['jquery'],
        'dataTables.bootstrap': ['jquery'],
        'dataTables.bootstraps.common': ['jquery'],
        'json2': ['jquery'],
        'serialize2obj': ['jquery'],
        'bootstrap-datetimepicker': ['jquery'],
        'bootstrap-datetimepicker.zh-CN': ['jquery'],
        'bootbox': ['jquery'],
        'icheck': ['jquery'],
        'bootstrap-treeview': ['jquery'],
        'jquery.form': ['jquery'],
        'ajaxfileupload': ['jquery'],
        'watermark': ['jquery'],

        'dist.common.common': ['jquery'],
        'dist.common.main': ['jquery'],
        'dist.common.api': ['jquery'],
        'dist.common.lib': ['jquery'],
        'dist.common.dt.util': ['jquery'],
        'dist.common.constants': ['jquery'],
        'dist.common.request': ['jquery'],
        'dist.common.tip': ['jquery'],
        'dist.common.bttree.util': ['jquery'],
        'dist.common.demo': ['jquery']
    },
    'urlArgs': '1453718967996',
    'waitSeconds': 0
});
