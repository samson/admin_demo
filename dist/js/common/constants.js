var TIP_MESSAGE = {

    TIP_TIME_OUT : 2000,

    MSG_WAITING : '请等待...',
    MSG_SUCCESS : '操作成功',
    MSG_FAIL : '操作失败',
    MSG_SERVER_ERROR : '服务器错误',
    NO_SERVER_DATA : '无服务器数据',

}

var CONFIG = {
    CERTIFICATION : 'CERTIFICATION'
}