/**
 * USER:    suncg
 * TIME:    2018-01-10 10:35
 * COMMENT: 公共工具对象
 */
var common = {

    /**
     * 从地址栏中获取参数
     * @param name
     * @returns {*}
     * @constructor
     */
    getQueryString: function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = window.location.search.substr(1).match(reg);
        if (r != null)
            return (r[2]);
        return null;
    },

    /**
     * 组装url
     * @param server
     * @param url
     * @returns {string}
     */
    getApiUrl: function (server, url) {
        return server + url + '?' + new Date().getTime();
    },

    /**
     * 验证是否为图片
     * @param ext
     * @returns {boolean}
     */
    validateImg: function (ext) {
        //ext = ext.toLowerCase();
        //"bmp","dib","gif","jfif","jpe","jpeg","jpg","png","tif","tiff","ico"
        return (ext && /^(jpg|jpeg|png|gif|bmp|dib|jfif|jpe|tif|tiff|ico)$/.test(ext));
    },

    /**
     * 获取扩展名
     * @param val
     */
    getExt: function (val) {
        if (!val)
            return "";
        var filename = val.replace(/.*(\/|\\)/, "");
        var fileExt = (/[.]/.exec(filename)) ? /[^.]+$/.exec(filename.toLowerCase()) : '';
        return fileExt;
    },

    /**
     * 格式化日期 yyyy-MM-dd
     * @param param
     * @returns {string}
     */
    getDateFormat: function (param) {
        if (!this.isNotBlank(param) || isNaN(param)) {
            return "";
        }
        if (param == 0) {
            return "";
        }
        var d = new Date(param);
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        var date = d.getDate();
        return year + "-" + (month < 10 ? "0" + month : month) + "-" + (date < 10 ? "0" + date : date);
    },

    /**
     * 格式化日期 yyyy-MM-dd HH:mm:ss
     * @param param
     * @returns {string}
     */
    getDateTimeFormat: function (param) {
        if (!this.isNotBlank(param) || isNaN(param) || param == 0) {
            return "";
        }
        var d = new Date(param);
        var year = d.getFullYear();
        var month = d.getMonth() + 1;
        var date = d.getDate();
        var hour = d.getHours();
        var minutes = d.getMinutes();
        var seconds = d.getSeconds();

        return year + "-" + (month < 10 ? "0" + month : month) + "-" + (date < 10 ? "0" + date : date) + " " + (hour < 10 ? "0" + hour : hour) + ":"
            + (minutes < 10 ? "0" + minutes : minutes) + ":" + (seconds < 10 ? "0" + seconds : seconds);
    },

    /**
     * 字符串是否为空白
     * @param button
     * @param status
     */
    isNotBlank: function (str) {
        if (typeof(str) == "undefined")
            return false;

        if (str == null)
            return false;

        if (str == "null")
            return false;

        if (str.length == 0)
            return false;

        return true;
    },

    isBlank: function (str) {
        return !this.isNotBlank(str);
    },

    /**
     * 字符串为空，返回空白
     * @param str
     */
    strIsBlank: function (str) {
        return this.isNotBlank(str) ? str : "";
    },

    /**
     * 获取元
     * @param num
     * @returns {string}
     */
    getPriceYuan: function (num) {
        if (num === '') {
            return null;
        }
        if (num < 0) {
            return parseFloat(-((0 - num) / 100).toFixed(2));
        }
        if (num > 0) {
            return parseFloat((num / 100).toFixed(2));
        } else {
            return 0;
        }
    },

    /**
     * 获取元, 不舍入
     * @param num
     * @returns {string}
     */
    getPriceYuans: function (num) {
        if (num === '') {
            return null;
        }
        if (num < 0) {
            return -((0 - num) / 100);
        }
        if (num > 0) {
            return (num / 100);
        } else {
            return 0;
        }
    },

    /**
     * 获取分
     * @param num
     * @returns {string}
     */
    getPriceFen: function (num) {
        if (num === '') {
            return null;
        }
        if (num < 0) {
            return -((0 - num) * 100).toFixed(0);
        }
        return (num * 100).toFixed(0);
    },

    /**
     * 使用a或者button的html标签传递数据 序列化
     * @param data 对象
     * @returns {*} 字符串
     */
    encodeData: function (data) {
        if (!data) {
            return '';
        }
        return encodeURI(JSON.stringify(data));
    },

    /**
     * 使用a或者button的html标签传递数据 饭序列化
     * @param data encode字符串
     * @returns {*} 对象
     */
    decodeData: function (data) {
        if (!data) {
            return '';
        }
        return JSON.parse(decodeURI(data));
    },

    urlencode: function (str) {
        str = (str + '').toString();
        return encodeURIComponent(str);
    },

    menu: function () {
        var current_href = window.location.href;
        // 左侧菜单高亮
        var currentMenu = $('.sidebar-menu a[href="' + current_href + '"]:first');
        currentMenu.closest('li').addClass('active');
        if (currentMenu) {
            var treeview = currentMenu.closest('.treeview');
            var ptreeview = currentMenu.closest('.ptreeview');
            var pptreeview = currentMenu.closest('.pptreeview');
            if (treeview.find('ul').length) {
                return treeview.find('a:first').trigger('click');
            } else {
                ptreeview.addClass('active').siblings().removeClass('active');
                pptreeview.addClass('active').siblings().removeClass('active');
                return currentMenu.siblings().removeClass('active');
            }
        }

    }

}





