var request = {
    /**
     * ajax post异步请求
     * @param url
     * @param reqData
     * @param successCallback 成功回调 status == 0
     * @param failCallback 失败回调 status != 0
     * @param serverFailCallback 服务器发生错误回调
     */
    ajaxPostJson: function (url, reqData, before, successCallback, failCallback, serverFailCallback) {
        $.ajax({
            "url": url,
            "data": reqData,
            "dataType": "json",
            "contentType": "application/json; charset=utf-8",
            "type": "post",
            "beforeSend": function (XMLHttpRequest) {
                if(typeof(before) === 'function'){
                    before(XMLHttpRequest);
                }
                //XMLHttpRequest.setRequestHeader("token", "");
            },
            "success": function (retData) {
                if (!retData) {
                    if (typeof(serverFailCallback) === 'function') {
                        serverFailCallback(TIP_MESSAGE.MSG_SERVER_ERROR);
                    }
                    return;
                }
                if (retData.status != 0) {
                    var msg = TIP_MESSAGE.MSG_FAIL;
                    if (retData.msg) {
                        msg = retData.msg;
                    }
                    if (typeof(failCallback) === 'function') {
                        failCallback(retData.status, retData.data, msg);
                    }
                    return;
                }

                if (typeof(successCallback) === 'function') {
                    var msg = TIP_MESSAGE.MSG_SUCCESS;
                    if (retData.msg) {
                        msg = retData.msg;
                    }
                    successCallback(retData.data, retData.msg);
                }
            },
            "error": function (e) {
                if (typeof(serverFailCallback) === 'function') {
                    serverFailCallback(TIP_MESSAGE.MSG_SERVER_ERROR);
                }
            }
        });
    },

    /**
     * ajax上传文件
     * @param uri
     * @param fileId
     * @param param
     * @param successCallback 成功回调 status == 0
     * @param failCallback 失败回调 status != 0
     * @param serverFailCallback 服务器发生错误或相应数据为空时回调
     */
    ajaxUpload: function (uri, fileId, param, successCallback, failCallback, serverFailCallback) {
        $.ajaxFileUpload({
            "url": uri,
            "secureuri": false,
            "data": param,
            "fileElementId": [fileId], //文件选择框的id属性
            "type": "post",
            "dataType": 'JSON',                   //服务器返回的格式，可以是json
            "success": function (retData, status) {
                if (!retData) {
                    if (typeof(serverFailCallback) === 'function') {
                        serverFailCallback(TIP_MESSAGE.MSG_SERVER_ERROR);
                    }
                    return;
                }
                if (retData.status != 0) {
                    var msg = TIP_MESSAGE.MSG_FAIL;
                    if (retData.msg) {
                        msg = retData.msg;
                    }
                    if (typeof(failCallback) === 'function') {
                        failCallback(retData.status, retData.data, msg);
                    }
                    return;
                }

                if (typeof(successCallback) === 'function') {
                    var msg = TIP_MESSAGE.MSG_SUCCESS;
                    if (retData.msg) {
                        msg = retData.msg;
                    }
                    successCallback(retData.data, retData.msg);
                }
            },
            error: function (data, status, e) {
                if (typeof(failCallback) === 'function') {
                    serverFailCallback(TIP_MESSAGE.MSG_SERVER_ERROR);
                }
            }
        });
    },

}