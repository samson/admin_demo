var tip = {
    /**
     * 成功
     * @param head
     * @param body
     */
    successToast: function (head, body) {
        if (!common.isNotBlank(head)) {
            head = TIP_MESSAGE.MSG_SUCCESS;
        }
        if (!common.isNotBlank(body)) {
            body = TIP_MESSAGE.MSG_SUCCESS;
        }
        $.toast({
            heading: head,
            text: body,
            position: 'top-right',
            icon: 'success',
            stack: false
        })
    }
    ,
    /**
     * 成功
     * @param head
     * @param body
     */
    successToastBody: function (body) {
        this.successToast(null, body);
    }
    ,

    /**
     * 错误
     * @param head
     * @param body
     */
    errorToast: function (head, body) {
        if (!common.isNotBlank(head)) {
            head = TIP_MESSAGE.MSG_FAIL;
        }
        if (!common.isNotBlank(body)) {
            body = TIP_MESSAGE.MSG_FAIL;
        }
        $.toast({
            heading: head,
            text: body,
            position: 'top-right',
            icon: 'error',
            stack: false
        })
    }
    ,
    /**
     * 错误
     * @param head
     * @param body
     */
    errorToastBody: function (body) {
        this.errorToast(null, body);
    }
    ,

    /**
     * 警告
     * @param head
     * @param body
     */
    warnToast: function (head, body) {
        if (!common.isNotBlank(head)) {
            head = '';
        }
        if (!common.isNotBlank(body)) {
            body = '';
        }
        $.toast({
            heading: head,
            text: body,
            position: 'top-right',
            icon: 'warning',
            stack: false
        })
    }
    ,
    /**
     * 警告
     * @param head
     * @param body
     */
    warnToastBody: function (body) {
        this.warnToast(null, body);
    }
    ,
    /**
     * 确认组件封装
     * @param msg
     * @param sureCallback
     * @param cancelCallback
     */
    confirm: function (msg, sureCallback, cancelCallback) {
        bootbox.confirm({
            message: msg,
            buttons: {confirm: {label: '确认'}, cancel: {label: '取消'}},
            callback: function (result) {
                // 返回 true  或者false
                if (result) {
                    if (typeof(sureCallback) === 'function') {
                        sureCallback();
                    }
                } else {
                    if (typeof(cancelCallback) === 'function') {
                        cancelCallback();
                    }
                }
            }
        });
    }
}