/**
 * USER:    suncg
 * TIME:    2018-01-10 10:35
 * COMMENT: bootstrap treeview封装工具
 */
var bttreeUtil = {

    /**
     * 选中父节点时，选中所有子节点
     * @param node
     * @returns {Array}
     */
    getChildNodeIdArr: function (node) {
        var ts = [];
        if (node.nodes) {
            for (x in node.nodes) {
                ts.push(node.nodes[x].nodeId);
                if (node.nodes[x].nodes) {
                    var getNodeDieDai = this.getChildNodeIdArr(node.nodes[x]);
                    for (j in getNodeDieDai) {
                        ts.push(getNodeDieDai[j]);
                    }
                }
            }
        } else {
            ts.push(node.nodeId);
        }
        return ts;
    },
    /**
     * 选中节点 所有父节点都选中
     * @param node
     */
    setParentNodeCheck: function (tree, node) {
        if (node && node.parentId != undefined) {
            var parentNode = tree.treeview("getNode", node.parentId);
            if (parentNode) {
                tree.treeview('checkNode', [node.parentId, {silent: true}]);
                this.setParentNodeCheck(tree, parentNode);
            }
        }

    },
    /**
     * 取消节点时 当父节点下所有子节点都取消 取消父节点
     * @param node
     */
    setParentNodeUnCheck: function (tree, node) {
        var parentNode = tree.treeview("getNode", node.parentId);
        if (parentNode.nodes) {
            var checkedCount = 0;
            for (x in parentNode.nodes) {
                if (parentNode.nodes[x].state.checked) {
                    checkedCount++;
                }
            }
            if (checkedCount == 0) {
                tree.treeview("uncheckNode", parentNode.nodeId);
                this.setParentNodeUnCheck(tree, parentNode);
            }
        }

    },
    /**
     * 取消父节点时 取消所有子节点
     * @param node
     * @returns {Array}
     */
    setChildNodeUncheck: function (node) {
        if (node.nodes) {
            var ts = [];    //当前节点子集中未被选中的集合
            for (x in node.nodes) {
                if (!node.nodes[x].state.checked) {
                    ts.push(node.nodes[x].nodeId);
                }
                if (node.nodes[x].nodes) {
                    var getNodeDieDai = node.nodes[x];
                    console.log(getNodeDieDai);
                    for (j in getNodeDieDai) {
                        if (!getNodeDieDai.nodes[x].state.checked) {
                            ts.push(getNodeDieDai[j]);
                        }
                    }
                }
            }
        }
        return ts;
    }

}