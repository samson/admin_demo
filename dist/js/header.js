
/**
 * 标题、简要说明. <br>
 * 类详细说明.
 * <p>
 * Copyright: Copyright (c) 2019-03-24 08:02
 * <p>
 * Company: samson
 * <p>
 *
 * @author sunchenguang
 * @email sunshine870830@126.com
 * @version 1.0.0
 */
$(function () {
    headerObj.initHeaderInfo();
})

var headerObj = {
    initHeaderInfo: function () {
        var header = $('#main-header-logo');
        var html =
            '<a href="/pages/index.html" class="logo">\n' +
            '<span class="logo-lg"><b>后台</b>管理系统</span>\n' +
            '</a>';
        header.append(html);
    }
}
