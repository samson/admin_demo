/**
 * 标题、简要说明. <br>
 * 类详细说明.
 * <p>
 * Copyright: Copyright (c) 2019-03-24 08:02
 * <p>
 * Company: samson
 * <p>
 *
 * @author sunchenguang
 * @email sunshine870830@126.com
 * @version 1.0.0
 */
$(function () {

    //初始化用户信息模块
    navObj.initUserInfo();

    //初始化菜单
    navObj.initMenu();

})

var navObj = {
    /**
     * 初始化菜单状态
     */
    initMenuStatus: function (navBar) {
        navBar.tree();
        navBar.filter(".menu-open").removeClass("menu-open");
        navBar.filter(".active").removeClass("active")
        navBar.find("a").each(function () {
            //debugger;
            var thiz = $(this);
            var windowLocation = String(window.location);
            var authpath = thiz.attr("authpath");
            if (windowLocation.indexOf(authpath) != -1) {
                var parent = thiz.parents('li');
                if (parent) {
                    parent.addClass("active");
                }
                parent = thiz.parents('.treeview');
                if (parent) {
                    parent.addClass("active");
                    parent.addClass("menu-open");
                }
            }
        });
    },

    /**
     * 初始化菜单
     * @returns {string}
     */
    initMenu: function () {
        var navBar = $('.sidebar-menu');
        var menuHtml = '';
        //导航头
        menuHtml = menuHtml + '<li class="header">导航菜单</li>';
        for(var i = 0 ; i < 1; i++){
            //导航项
            menuHtml = menuHtml + this.initMenuItem(null);
        }
        navBar.append(menuHtml);
        //初始化菜单选中状态
        this.initMenuStatus(navBar);
    },

    initMenuItem: function(data){
        var hasChildren = true;
        var menuHtml = '<li class="treeview">';
        if(!hasChildren){
            //无子节点
            menuHtml = menuHtml +
                '<a href="/pages/website/website1.html"><i class="fa fa-link"></i> <span>菜单2</span></a>';
        }else{
            //有子节点菜单
            menuHtml = menuHtml + '<a href="#"><i class="fa fa-link "></i> <span>权限管理</span>';
            menuHtml = menuHtml + '<span class="pull-right-container">';
            menuHtml = menuHtml + '<i class="fa fa-angle-left pull-right"></i>';
            menuHtml = menuHtml + '</span>';
            menuHtml = menuHtml + '</a>';
            menuHtml = menuHtml + '<ul class="treeview-menu">';
            menuHtml = menuHtml + '<li><a href="/auth/code/index.html" authpath="/auth/code">' +
                '<i class="fa fa-circle-o"></i>权限码管理</a></li>';
            menuHtml = menuHtml + '<li><a href="/auth/role/index.html" authpath="/auth/role">' +
                '<i class="fa fa-circle-o"></i>角色管理</a></li>';
            menuHtml = menuHtml + '<li><a href="/auth/user/index.html" authpath="/auth/user">' +
                '<i class="fa fa-circle-o"></i>用户管理</a></li>';
            menuHtml = menuHtml + '</ul>';
        }
        menuHtml = menuHtml + '</li>';
        return menuHtml;

    },

    /**
     * 初始化用户信息
     * @returns {string}
     */
    initUserInfo: function () {
        var userPanel = $('.user-panel');
        var userInfoHtml = '';
        userInfoHtml = userInfoHtml + '<div class="pull-left image">';
        //userInfoHtml = userInfoHtml + '<img data-src="holder.js/50x50?text=张&random=yes&fg=&size=15&font=Helvetica" class="img-circle" >';
        userInfoHtml = userInfoHtml + '<img src="" onerror=this.src="/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">';
        userInfoHtml = userInfoHtml + '</div>';
        userInfoHtml = userInfoHtml + '<div class="pull-left info">';
        userInfoHtml = userInfoHtml + '<p>张三(201188)</p>';
        userInfoHtml = userInfoHtml + '<a href="#"><i class="fa fa-circle text-success"></i>正式环境</a>';
        userInfoHtml = userInfoHtml + '<a href="#"><i class="fa fa-power-off"></i>注销</a>';
        userInfoHtml = userInfoHtml + '</div>';
        userPanel.append(userInfoHtml);
    }

}
