Namespace.register("com.guazi.paym.auth.website");
com.guazi.paym.auth.website = {

    f1: function (url) {
        return '111';
    }

}
$(function () {

    $('#example1').DataTable({
        'serverSide': true, //启用服务器端分页
        'paging': true,
        'lengthChange': true,
        'searching': false, //禁用原生搜索
        'ordering': true,
        'info': true,
        'autoWidth': true, //禁用自动调整列宽
        'orderMulti ': false, //启用多列排序
        "sScrollX": "100%",
        "sScrollXInner": "200%",
        "bScrollCollapse": true,
        'order:': [2], //取消默认排序查询,否则复选框一列会出现小箭头
        'renderer': "bootstrap",  //渲染样式：Bootstrap和jquery-ui
        'processing': true,  //隐藏加载提示,自行处理
        'columnDefs': [{"bSortable": false, "aTargets": [2, 3]}],
        'aaSorting': [[1, "desc"]],
        'ajax': function (data, callback, settings) {
            //封装请求参数
            var param = dtutil.createDtPageParam(data);
            request.ajaxPostJson(api.a.a1, JSON.stringify(param), null, function (respData, respMsg) {
                var returnData = dtutil.createReturnData(data.draw);
                returnData.recordsTotal = respData.total;//返回数据全部记录
                returnData.recordsFiltered = respData.total;//后台不实现过滤功能，每次查询均视作全部结果
                var datas = new Array();
                var responseData = respData.data;
                if (responseData) {
                    for (var i = 0; i < responseData.length; i++) {
                        var row = new Array();
                        var item = responseData[i];
                        row.push(item.a + "dsafjklsfjskldadsfdsfsdfdsfsadffjsdkfjksafjaskjfkkl");
                        row.push(item.b + "dsafjklsfjskldadsfdsfsdfdsfsadffjsdkfjksafjaskjfkkl");
                        row.push(item.c + "dsafjklsfjskldadsfdsfsdfdsfsadffjsdkfjksafjaskjfkkl");
                        row.push(item.d + "dsafjklsfjskldadsfdsfsdfdsfsadffjsdkfjksafjaskjfkkl");
                        row.push(item.e + "dsafjklsfjskldadsfdsfsdfdsfsadffjsdkfjksafjaskjfkkl");
                        datas.push(row);
                    }
                }
                returnData.data = datas;
                callback(returnData);
            }, function (respStatus, respData, respMsg) {
                var returnData = dtutil.createReturnData(data.draw);
                callback(returnData);
                tip.errorToastBody(respMsg);
            }, function (respMsg) {
                var returnData = dtutil.createReturnData(data.draw);
                callback(returnData);
                tip.errorToastBody(respMsg);
            })
        },
        'rowCallback': function (row, data) {
            var $row = $(row);
            $row.find("a.edit").unbind("click").click(function () {
                var thiz = $(this);
            });
        }
    });

    //panel=======================================================
    var panel = $("#modal-default");
    //panel显示事件
    panel.on('show.bs.modal', function () {
        alert("shown");
    });
    //panel关闭事件
    panel.on('hidden.bs.modal', function () {
        alert("hidden");
    });
    //=======================================================
    //datetimepicker=======================================================
    //选择小时分钟
    //$('#datepicker').datetimepicker({
    //
    //});

    //年月日
    $('#datepicker').datetimepicker({
        format: 'yyyymmdd',
        weekStart: 1,
        autoclose: true,
        startView: 2,
        minView: 2,
        forceParse: false,
        language: 'zh-CN'
    });

    //选择月
    //$('#datepicker').datetimepicker({
    //    format: 'yyyymm',
    //    weekStart: 1,
    //    autoclose: true,
    //    startView: 3,
    //    minView: 3,
    //    forceParse: false,
    //    language: 'zh-CN'
    //});
    //选择年
    //$('#datepicker').datetimepicker({
    //    format: 'yyyymm',
    //    weekStart: 1,
    //    autoclose: true,
    //    startView: 4,
    //    minView: 4,
    //    forceParse: false,
    //    language: 'zh-CN'
    //});
    $('.timepicker').datetimepicker({
        format: 'hh:ii',
        startView: 0,
        autoclose: true,
        minView: 0,
        minuteStep: 1
    });
    //=======================================================
    //confirm toast=======================================================
    $("#button-confirm").on("click", function () {
        tip.confirm("ss",
            function () {
                tip.successToast("ok", "ok")
            },
            function () {
                tip.errorToast("fail", "fail")
            })
    });
    //=======================================================
    //iCheck for checkbox and radio inputs=======================================================
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    })
    //=======================================================
    //treeview
    //result 中code: "213213"是自己添加的
    var result =
        [
            {
                code: "213213",
                text: "Parent 1",
                nodes: [{
                    code: "213213", text: "Child 1",
                    nodes: [{code: "213213", text: "Grandchild 1"}, {code: "213213", text: "Grandchild 2"}]
                }, {code: "213213", text: "Child 2"}]
            },
            {code: "213213", text: "Parent 2", state: {checked: true}},
            {code: "213213", text: "Parent 3"},
            {code: "213213", text: "Parent 4"},
            {code: "213213", text: "Parent 5"}
        ];
    var treeView = $('#tree').treeview({
        data: result,         // 数据源
        showCheckbox: true,   //是否显示复选框
        highlightSelected: true,    //是否高亮选中
        nodeIcon: 'glyphicon glyphicon-globe',//节点上的图标
        emptyIcon: '',    //没有子节点的节点图标
        multiSelect: false,    //多选
        onMouseDown: function (event, node) {
            if (event.which === 3) {
                //鼠标右键 获取不到node
                alert(11);
            }
        },
        onNodeSelected: function (event, node) {
            if (event.which === 3) {
                //鼠标右键
                alert(11);
            }
        },
        onNodeChecked: function (event, node) {
            var thiz = $(this);
            var childNodes = bttreeUtil.getChildNodeIdArr(node);
            if (childNodes) {
                //选中所有子节点
                thiz.treeview('checkNode', [childNodes, {silent: true}]);
            }
            //选中父节点
            bttreeUtil.setParentNodeCheck(thiz, node);
        },
        onNodeUnchecked: function (event, node) {
            //取消选中节点
            var thiz = $(this);
            var childNodes = bttreeUtil.getChildNodeIdArr(node);
            if (childNodes) {
                //取消父节点 子节点取消
                thiz.treeview('uncheckNode', [childNodes, {silent: true}]);
            }
            //判断父节点是否需要取消
            bttreeUtil.setParentNodeUnCheck(thiz, node);
        }
    });
    $("#getTreeViewValue").on("click", function () {
        var checkedNodes = $('#tree').treeview("getChecked");
        alert(checkedNodes[0].code);
    });

});







