/**
 * 标题、简要说明. <br>
 * 类详细说明.
 * <p>
 * Copyright: Copyright (c) 2019-03-24 08:02
 * <p>
 * Company: samson
 * <p>
 *
 * @author sunchenguang
 * @email sunshine870830@126.com
 * @version 1.0.0
 */
$(function () {
    footerObj.initFooterInfo();
})

var footerObj = {
    initFooterInfo: function () {
        var footer = $('.main-footer');
        var html =
            '<div class="pull-right hidden-xs">' +
            'Version 1.0' +
            '</div>' +
            '<strong>Copyright &copy; 2019 <a href="https://www.samson.com">samson</a>.' +
            '</strong> All rights reserved.';

        footer.append(html);
    }
}